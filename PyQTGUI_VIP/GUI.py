import sys
import get_data
import transmit_data
import serial

from PyQt5 import QtCore, QtGui, QtWidgets

path_of_image = 'galaxy.jpg'
alt_image_path = 'planet.jpeg'




class App(QtWidgets.QWidget):
    def __init__(self, serial_port):
        super().__init__()
        self.counter = 0
        self.title = 'Ultrasound GUI'
        self.left = 10
        self.top = 10
        self.width = 1920
        self.height = 1080
        self.initUI()
        self.serial_port = serial_port

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.label = QtWidgets.QLabel(self)
        self.label.move(280,75)

        # create a timer for updating images.
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_image)
        timer.start(200)  # sets timer to take a new reading every 200 ms
        self.update_image()

    def update_image(self):
        print(transmit_data.get_voltage_reading_and_coordinates(ser))
        try:
            z_coord, x_coord, y_coord = transmit_data.get_voltage_reading_and_coordinates(ser)
        except Exception:
            z_coord, x_coord, y_coord = (0,14,0)

        self.counter = self.counter + 1
        path = 'ultrasound_images/x_' + str(x_coord) + '/y_' + str(y_coord) + '/test.png'
        if(self.counter > 1):
            self.counter = 0
            pixmap = QtGui.QPixmap(path)
        else:
            pixmap = QtGui.QPixmap(path)
            
        if not pixmap.isNull():
            self.label.setPixmap(pixmap)
            self.label.adjustSize()
            #self.resize(pixmap.size())
            self.label.setAlignment(QtCore.Qt.AlignCenter)


if __name__ == '__main__':
    serial_port = transmit_data.get_COM_port()
    #print('SERIAL: ', serial_port)
    #try:
    ser = serial.Serial(serial_port, 115200)
    #except Exception:
    #    pass

    print('SER: ', ser)
    app = QtWidgets.QApplication(sys.argv)
    ex = App(ser)
    ex.show()
    sys.exit(app.exec_())



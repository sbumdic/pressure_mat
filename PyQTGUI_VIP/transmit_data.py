import serial
from serial import Serial
from PIL import Image
import numpy as np

import serial.tools.list_ports

def get_COM_port():
    serial_port = 'COM5'
    ports = list(serial.tools.list_ports.comports())
    for p in ports:
        if "Arduino" in p.description:
            serial_port = p.description
            serial_port = serial_port[serial_port.find("(")+1:serial_port.find(")")]
            return serial_port
    return serial_port

def get_voltage_reading_and_coordinates(ser):

    baud_rate = 115200 #In arduino, Serial.begin(baud_rate)

    try:
        if ser.in_waiting:  # If there is serial data available
            line = ser.readline()
            line = line.decode("ANSI") #ser.readline returns a binary, convert to string

            if(len(line) > 7 and line.startswith("Calibrating")==False):
                 max_value, x_coord, y_coord = line.split(" ")
                 y_coord = y_coord.split()[0]
                 return int(max_value), int(x_coord), int(y_coord)
        else:
            return 2,4,6

    except Exception as e:
        print(e)
        return 2,4,6

    